const express = require('express');
const router = express.Router();

let users = require('../data/users');

router.get('/', (req, res) => {
	res.json(users);
});

router.get('/:userName', (req, res) => {
  let user = users.find(user => req.params.userName === user.userName);
	res.json(user);
});

router.post('/', (req, res) => {
	users.push(req.body);
	res.json(users);
});

module.exports = router;
