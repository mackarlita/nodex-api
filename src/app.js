const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const morganDebug = require('morgan-debug');
const helmet = require('helmet');

const routes = require('./routes/index');

const app = express();

app.use(helmet());
app.use(morganDebug('myapp:server','dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());

app.get('/', (req, res) => {
	res.send('App works!!!!!');
});

app.use('/api', routes);

app.get('*', (req, res) => {
	res.send('-- Undefined route --');
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
	next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
	next(createError(500, err));
});

module.exports = app;
