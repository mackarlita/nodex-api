const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "api.bundle.js"
    },
    target: "node",
    plugins: [
        new CleanWebpackPlugin()
    ]
};